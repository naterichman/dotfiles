# Uses:

## Programs

* [alacritty](./.config/alacritty/alacritty.yml)
* [git](./.gitconfig)
    * [dandavison/delta](https://github.com/dandavison/delta)
* [htop](./.config/htop/htoprc)
* [i3](./.config/i3/config) (window manager)
* [polybar](./.config/polybar/config) (status bar)
* [picom](./.config/picom.conf) (compositor)
* [rofi](./.config/wal/templates/colors-rofi-dark.rasi)
* [tmux](./.tmux.conf)
    * Borrowed some config from: [gpakosz/.tmux](https://github.com/gpakosz/.tmux)
* [neovim](./.config/nvim/init.vim)
    * [junegunn/vim-plug](https://github.com/junegunn/vim-plug)
* [zsh](./.zshrc)
    * [ohmyzsh](https://github.com/ohmyzsh/ohmyzsh)

## General
* [dylanaraps/pywal](https://github.com/dylanaraps/pywal)
* [powerline/powerline](https://github.com/powerline/powerline)
