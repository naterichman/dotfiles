""""""" Configure virtualenv for nvim
" Install pyenv virtualenv plugin
" pyenv virtualenv 3.7.9 py3nvim
" pyenv activate
" python3 -m pip install pynvim jupyter
let g:python3_host_prog = '/home/naterichman/.pyenv/versions/py3nvim/bin/python'
"python3 import os; import vim
"python3 activate_this = '/home/nrichman/.pyenv/versions/py3nvim/bin/activate_this.py'
"python3 with open(activate_this) as f: exec(f.read(), {'__file__': activate_this})

" Need to have before the rest of the config
let mapleader = "," 		    	" Better mapleader

" Plugins
call plug#begin('~/.config/nvim')
"UI plugins
  Plug 'preservim/nerdtree' 			"File tree plugin
  Plug 'mbbill/undotree'                "Undo visualizer
  Plug 'vim-airline/vim-airline'        "Statusline
  Plug 'flazz/vim-colorschemes'			"Colorschemes
  Plug 'airblade/vim-gitgutter'			"Git diff visualizer

"Interaction plugins
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'yuttie/comfortable-motion.vim' 	"Comfortable motion/flicking

"Integrations
  Plug 'jupyter-vim/jupyter-vim'        "Vim Jupyter integration
  Plug 'tpope/vim-fugitive'			    "Git integration
  Plug 'iamcco/markdown-preview.nvim'	"Markdown previews.
  Plug 'elzr/vim-json'				    "JSON viewer

"Language helpers
  Plug 'tmhedberg/SimpylFold'			"Better python folding
call plug#end()

" COC config
nmap <leader>g <Plug>(coc-definition)
nmap <leader>c :CocList commands<CR>
nmap <leader>l <Plug>(coc-codeaction-line)
xmap <leader>l <Plug>(coc-codeaction-selected)
nmap <leader>r <Plug>(coc-references)
nmap <leader>f :call CocAction('format')<CR>

autocmd CursorHold * silent call CocActionAsync('highlight')
nnoremap <silent> K :call <SID>show_documentation()<CR>
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction


" Undotree config
nnoremap <F5> :UndotreeToggle<CR>

" Nerdtree config
" Jedi likes to overwrite this.
au VimEnter * nnoremap <leader>n :NERDTreeFocus<CR>

" Jupyter vim config
vmap <leader>e <Plug>JupyterRunVisual

" Ultisnips config
let g:UltiSnipsExpandTrigger="<F5>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:ultisnips_python_style="google"

"""""" Borrowed from amix/vimrc """"""""""
source ~/.config/nvim/helpers.vim   " Helper functions
set history=500 			        " Sets how many lines of history VIM has to remember
filetype plugin on 			        " Filetype identification
filetype indent on
nmap <leader>w :w!<cr>			" Fast saving

" Sudo save:
command! W execute 'w !sudo tee % > /dev/null' <bar> edit!
set wildmenu				    " Command mode completions
set wildignore=*.o,*~,*.pyc		" Ignore pyc *.o and *~ files.
set ruler				        " Show current position
set hid					        " Hide buffer when inactive
set backspace=eol,start,indent 	" Configure backspace so it acts as it should act
set whichwrap+=<,>,h,l          " Keys which move left and right
set ignorecase				    " Ignore case when searching
set smartcase				    " Override ignorecase when search term contains uppers.
set hlsearch				    " Highlight results.
set magic				        " regex
set showmatch 				    " Show matching brackets

" No annoying sound on errors
set noerrorbells
set novisualbell
set tm=500
set foldcolumn=1			    " Set fixed 1 column to show folds
syntax enable
set nu					        " Line numbers
set encoding=utf8			    " UTF-8 encoding
set ffs=unix,dos,mac			" Default unix files.

" Turn backup off, since most stuff is in SVN, git etc. anyway...
set nobackup
set nowb
set noswapfile
set expandtab				" Use spaces instead of tabs
set smarttab				" Better tabs
set shiftwidth=4			" Tab size
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500
set ai 					"Auto indent
set si 					"Smart indent
set wrap 				"Wrap lines

" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
" VisualSelection defined in ~/.config/nvim/helpers.vim
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>
" search and replace the selected text
vnoremap <silent> <leader>r :call VisualSelection('replace', '')<CR>

" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove 
map <leader>t<leader> :tabnext

" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()

" Return to last edit position when opening files.
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Remove extra spaces before save
" CleanExtraSpaces defined in ~/.config/nvim/helpers.vim
if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif

map <leader>ss :setlocal spell!<cr> 			" Toggle local spell checking

" Ignore files.
let NERDTreeIgnore=['\.pyc$', '\.o$', '\~$'] "ignore files in NERDTree

" General editor
set textwidth=88
set foldlevel=99
set foldmethod=indent

" Python
let python_highlight_all = 1
au FileType python syn keyword pythonDecorator True None False self
au FileType python set foldmethod=indent |
	\ set foldlevel=99 |
    \ set expandtab |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set colorcolumn=88 |
    \ set textwidth=79 |

" Markdown
au FileType markdown set textwidth=88

" JSON
au FileType json set foldmethod=indent |
  \ set foldlevel=99 |
  \ set tabstop=2 |
  \ set shiftwidth=2 | 

" Rust
au FileType rust set foldmethod=syntax |
  \ set foldlevel=99 |
  \ set tabstop=4 |
  \ set shiftwidth=4 | 
au BufNewFile,BufRead *.py
    \ set textwidth=100 |

" Must have `ln -s ~/.cache/wal/colors-wal.vim ~/.config/nvim/colors/colors-wal.vim`
" to use.
set t_ut=
set background=dark
set termguicolors
colo Atelier_EstuaryDark


" Undo file.
set undofile
set undodir=~/.cache/nvim/undofiles
