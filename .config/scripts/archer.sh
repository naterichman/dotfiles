#!/bin/bash
export GTK_THEME='Arc-Dark'
alias nsh='. ~/.config/scripts/ssh_help.sh'
alias graphics='sh /home/nrichman/.config/scripts/graphics.sh'
alias wpa_cli='sudo wpa_cli -i wlp14s0'
alias left='sh ~/.config/scripts/left.sh'
alias i3lock='i3lock -i ~/Pictures/Windows_background.png -p win'
