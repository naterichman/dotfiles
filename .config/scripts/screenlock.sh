#!/bin/zsh

import -window root ~/Pictures/lock.jpg
convert ~/Pictures/lock.jpg -blur 0x6 ~/Pictures/lock_blur.png
flip=$(shuf 0-1 -n 1)
sh ~/.config/scripts/glitch.sh $flip
i3lock -e -i /tmp/lock.png --timepos='x+110:h-70' --datepos='x+43:h-45' --clock --date-align 1 --indpos='x+280:h-70' \
	--radius=25 --ring-width=4 --force-clock --pass-media-keys --veriftext='' --wrongtext=''
