export ZSH=$HOME/.oh-my-zsh

# Used for git prompt syling
ZSH_THEME="robbyrussell"

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Plugins
plugins=(
  docker
  git
  history-substring-search
  shrink-path
  zsh-autosuggestions
  zsh-vim-mode
)

source $ZSH/oh-my-zsh.sh
bindkey -v
export KEYTIMEOUT=2
export PATH=$PATH:~/.local/bin

# export ARCHFLAGS="-arch x86_64"
#
# Useful aliases
alias mkdir='mkdir -p'
alias ppath='export PYTHONPATH=$(pwd)'
alias vim='nvim'
alias ls='exa'
alias cat='batcat'
alias vtop='vtop -t colors-vtop'
alias catnc='sh ~/.config/scripts/no_comments.sh'
alias gotop='gotop --color=monokai -s'
alias console='jupyter qtconsole --style monokai &'
alias tmux='tmux -2'
alias screen="~/.config/scripts/screenrecord.sh"
alias kc="kubectl"

# Pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init - 2>&1 > /dev/null)"
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
export POETRY_VIRTUALENVS_IN_PROJECT=true

# Editor
export VISUAL=nvim
export EDITOR="$VISUAL"

# Set custom prompt
setopt PROMPT_SUBST
RPS1='${MODE_INDICATOR_PROMPT} ${vcs_info_msg_0_}'
PS1='%(?:%{%}➜ :%{%}➜ ) %{$fg[blue]%}$(shrink_path -f)%{$reset_color%} $(git_prompt_info)$ '

# Source credentials files.
source $HOME/.config/scripts/credentials.sh
export FLYWHEEL_CLI_BETA=true
export DEBIAN_PREVENT_KEYBOARD_CHANGES=yes

export PATH="$HOME/.poetry/bin:$PATH"

eval "$(zoxide init zsh)"
eval "$(rgm-bin init zsh)"
